angular.module('PatientRecord.doctor', []).directive('goBack', function($ionicHistory) {
    return {
        restrict: "A",
        link: function(scope,ele,attr){
            ele.on('click',function(){
                $ionicHistory.goBack();
            })
        }
    }
}).directive('sgMaxLength',function(){
    var link=function(scope,ele,attr,ctrls){
        var mxLength=!isNaN(attr.sgVal)?parseInt(attr.sgVal):30;
        ele.on('keyup',function(e){
            ctrls.$parsers.push(function(el){
                if(el.length>mxLength){
                    var mxVal=el.substr(0,mxLength);
                    ctrls.$setViewValue(mxVal);
                    ctrls.$render();
                    return mxVal;
                }
                return el;
            })
        })
    }
    return{
        restrict:"A",
        link:link,
        require:"ngModel"
    }
});