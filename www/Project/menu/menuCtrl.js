angular.module('PatientRecord.menuCtrl',[])
.controller('menuCtrl', function($scope, $state, $ionicLoading, user, customService, $rootScope, global, localDB) {

    $scope.$on("$ionicView.beforeEnter", function() {
        $scope.homeHeaderClr = "#00d8ff";
        //console.log("userDBInfo=>>  "+JSON.stringify(customService.userDBInfo));
        $scope.userInfo = customService.userDBInfo;        
    });
    
    $scope.$on('carouselChange', function (event, args) {
        //console.log("change event called=>>  "+JSON.stringify(args));
        user.fetchUserInfo().then(function(objS){
            customService.userDBInfo = objS; 
            $scope.userInfo = customService.userDBInfo;               
        },function(objE){});
    });

    $scope.logout = function() {
        customService.confirm('Are you sure, you want to logout?',$scope.userLogout);
    }
            
    $scope.userLogout = function(index) {
        if(index==2) {
            localStorage.setItem('loginStatus','No');
            localStorage.setItem('loginUserId','');
            customService.basicPSave = false;
            customService.allergiesSave = false;
            customService.preExistingSave = false;
            customService.userDBInfo = '';
            customService.uploadRecID = '';
            customService.objRSP={};
            $state.go('login');
            /*localDB.deleteDB().then(function(res){
               console.log('db deleted');
                localStorage.setItem('loginStatus','No');
                localStorage.setItem('loginUserId','');
                customService.basicPSave = false;
                customService.allergiesSave = false;
                customService.preExistingSave = false;
                customService.userDBInfo = '';
                $state.go('login');
            },function(){
                //err 
            })*/
            
            //            var logoutData = {
            //                "user_id": customService.userId,
            //                "device_token":device.uuid
            //            }
            //            console.log('logoutData:- '+JSON.stringify(logoutData));
            //            $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
            //            user.postMethod(logoutData, 'logout').then(function(objS) {
            //                console.log('logoutData success:- '+JSON.stringify(objS));
            //                $ionicLoading.hide();
            //                if (objS.responseCode == '200') {
            //                    localStorage.setItem('loginStatus','No')
            //                    localStorage.setItem('loginUserId','')
            //                    $state.go('login')
            //                }
            //            }, function(objE) {
            //                $ionicLoading.hide();
            //                if(objE.msg.responseCode==400){
            //                    customService.ToShowAlert(objE.msg.responseMessage);
            //                } else {
            //                    customService.ToShowAlert('Network error.');
            //               }
            //            })
        } else {
        }
    }
            
    $scope.setting = function() {
        $rootScope.goFromHome = true;
        $state.go('setting');
        //window.plugins.nativepagetransitions.slide({"direction":"left"},function (msg) {},function (msg) {});
    }
            
    $scope.isShowHome = false;
    $scope.isShowProvider = false;
    $scope.isShowFNF = false;
    $scope.isShowTimeline = false;
    $scope.isShowProfile = false;
    
    $scope.openHome = function() {
        $scope.isShowProvider = false;
        $scope.isShowFNF = false;
        $scope.isShowTimeline = false;
        $scope.isShowProfile = false;
        $scope.isShowHome == false ? $scope.isShowHome = true : $scope.isShowHome = false;
    }
    $scope.openProvider = function() {
        $scope.isShowHome = false;
        $scope.isShowFNF = false;
        $scope.isShowTimeline = false;
        $scope.isShowProfile = false;
        $scope.isShowProvider == false ? $scope.isShowProvider = true : $scope.isShowProvider = false;
    }
    $scope.openFNF = function() {
        $scope.isShowHome = false;
        $scope.isShowProvider = false;
        $scope.isShowTimeline = false;
        $scope.isShowProfile = false;
        $scope.isShowFNF == false ? $scope.isShowFNF = true : $scope.isShowFNF = false;
    }
    $scope.openTimeline = function() {
        $scope.isShowHome = false;
        $scope.isShowProvider = false;
        $scope.isShowFNF = false;
        $scope.isShowProfile = false;
        $scope.isShowTimeline == false ? $scope.isShowTimeline = true : $scope.isShowTimeline = false;
    }    
    $scope.openProfile = function() {
        $scope.isShowHome = false;
        $scope.isShowProvider = false;
        $scope.isShowFNF = false;
        $scope.isShowTimeline = false;
        $scope.isShowProfile == false ? $scope.isShowProfile = true : $scope.isShowProfile = false;
    }
    
    $scope.navHome = function() {
        $state.go('menu.home');
    }
    
    $scope.timeline = function() {
        global.timelineId="";
        $state.go('timeline');
    }
    
    $scope.calendar = function() {
        customService.preStateForWHW = "menu";
        $state.go('calender');
    }

    $scope.sahre = function() {
        customService.objectForShare=[];
        customService.stateForShare = "menu.home";
        $state.go('share');
    }
            
    $scope.upload = function() {
        //$state.go('upload');
    }
        
    $scope.personalInfo = function() {
        $state.go('personal');
    }
        
    $scope.myProvider = function() {
        $rootScope.goFromHome = true;
        $state.go('provider');
    }
            
    $scope.healthCond = function() {
        $rootScope.goFromHome = true;
        $state.go('healthConditions');
    }
            
    $scope.medication = function() {
        $rootScope.goFromHome = true;
        $state.go('medication');
    }
        
    $scope.allergyDis = function() {
        $rootScope.goFromHome = true;
        $state.go('allergyPatient');
    }
            
    $scope.vaccineDocument = function() {
        $state.go('vaccineDocument');
    }
            
    $scope.vitalSymptoms = function() {
        $state.go('vitalSymptoms');
    }
            
    $scope.emergencyContact = function() {
        //$state.go('emergencyContact');
        $state.go('emergencyContactView');
    }
            
    $scope.familyFriend = function() {
        //$state.go('familyFriend');
    }
        
    $scope.reportPrescription = function() {
        customService.objRSP={};
        $state.go('reportPrescription');
    }

    $scope.addNewProvider = function(){
        if($scope.userInfo.accessStatus==1){
            $rootScope.goFromHome = true;
            customService.preStateForAddProvider = "menu";
            $state.go('addProvider');
        }
    }

    $scope.myQRCode = function(){
        if($scope.userInfo.accessStatus==1){
            $rootScope.goFromHome = true;
            $state.go('myQRCode');
        }
    }

    $scope.manageLinkedCont = function(){
        customService.fandFActiveTab="mngLinkTab";
        $state.go('familyFriend');
    }

    $scope.moreContacts = function(){
        customService.fandFActiveTab="moreContTab";
        $state.go('familyFriend');
    }

    $scope.inviteContacts = function(){
        customService.fandFActiveTab="inviteContTab";
        $state.go('familyFriend');
    }

    $scope.additionalInfo = function(){
        $rootScope.goFromHome = true;
        customService.preStateForAddInfo = "menu";
        $state.go('additionInformation');
    }

    $scope.weight = function(){
        console.log("accessStatus=>>  "+$scope.userInfo.accessStatus);
        if($scope.userInfo.accessStatus!=-1){
            $rootScope.goFromHome = true;
            customService.preStateForWHW = "menu";
            $state.go('weight');
        }
    }

    $scope.height = function(){
        if($scope.userInfo.accessStatus!=-1){
            $rootScope.goFromHome = true;
            customService.preStateForWHW = "menu";
            $state.go('height');
        }
    }

    $scope.waist = function(){
        if($scope.userInfo.accessStatus!=-1){
            $rootScope.goFromHome = true;
            customService.preStateForWHW = "menu";
            $state.go('waist');
        }
    }

});
