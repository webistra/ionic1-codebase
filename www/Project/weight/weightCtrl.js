angular.module('PatientRecord.weightCtrl',[])
.controller('weightCtrl', function($scope, $state, $ionicHistory, customService, user, $ionicLoading, customService, $ionicPopup, $ionicPopover, $timeout, disease, $rootScope, $filter, $timeout, $ionicScrollDelegate, $location,global) {    
    $scope.$on("$ionicView.beforeEnter", function() {
        $scope.user = {"userId":customService.userDBInfo.userId,"kg":"","weightLbs":"","note":"","recordate":new Date(),"fromDate":undefined,"toDate":undefined};
        $scope.notesLbl = true;
        $scope.editNotesLbl = false;
        $scope.weightData = [];
        $scope.userName = customService.userDBInfo.fullName;
        $scope.onHoldPressed = false;
        $scope.arrForShare = [];
        $scope.shareBtn = true;
        $scope.moreBtn = false;
        $scope.oneWFilter = false;
        $scope.oneMFilter = false;
        $scope.threeMFilter = false;
        $scope.sixMFilter = false;
        $scope.oneYFilter = false;
        $scope.userInfo = angular.copy(customService.userDBInfo);
        if(customService.userDBInfo.image==null || customService.userDBInfo.image==""){
            $scope.userimage = 'img/profile_pic.png';
        } else { 
            $scope.userimage = customService.userDBInfo.image;
        }
        $scope.settingData = {};
        disease.getPatientSettingData(localStorage.getItem('loginUserId')).then(function(objS){
            console.log("getPatientSettingData_succ->>  "+JSON.stringify(objS));              
            $scope.settingData = objS;           
        },function(objE){
            console.log("getPatientSettingData_err->>  "+JSON.stringify(objE));
        });
        $scope.getWeightData();
        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
        $timeout(function(){
            var id=$('.date-time_bp-P').attr('id');
            $('#'+id)[0].value=$filter('dateWithTime')($scope.user.recordate.getTime());        
            $ionicLoading.hide();
        },2000);
    });
    
    $scope.getWeightData = function() {
        user.fetchWeight().then(function(res){
            //console.log('fetchWeight_succ-->:  '+JSON.stringify(res));
            $scope.graphRecord = [['Entry', 'Weight']];
            //$scope.weightData = res;
            $scope.weightData = [];
            for(var i=0; i<res.length; i++){
                if(res[i].kg!="")
                    $scope.weightData.push(res[i]);
            }
            for(i=0;i<$scope.weightData.length;i++) {
                var timeValue=new Date($scope.weightData[i].recordDate);
                var timeFormat=timeValue.getDate()+'-'+(timeValue.getMonth()+1)+'-'+timeValue.getFullYear();
                //var timeFormat=timeValue.getDate();
                $scope.graphRecord.push([timeFormat,parseInt($scope.weightData[i].kg)]);
            }
            for(var i=0; i<$scope.weightData.length; i++){
                $scope.weightData[i].selected = false;
            }
            //console.log('Weight Data Arr->> '+JSON.stringify($scope.weightData));
            if($scope.graphRecord.length>1) {
                $timeout(function(){
                    google.charts.load('current', {'packages':['corechart']});
                    google.charts.setOnLoadCallback(drawChart);                    
                },1000);
            }
            function drawChart() {
                var data = google.visualization.arrayToDataTable($scope.graphRecord);
                var options = {
                    title: 'Weight Record',
                    hAxis: {title: 'Time',  
                        titleTextStyle: {color: '#333'},
                        direction: -1, 
                        slantedText: true, 
                        slantedTextAngle: 90
                    },
                    vAxis: {title: 'Kg',minValue: 0,gridlines: {count: 9}},
                    legend: {position: 'none'}
                };
                var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                chart.draw(data, options);
            } 
            $location.hash(global.weightId);
            $ionicScrollDelegate.$getByHandle('weightList').anchorScroll(true);           
        },function(err){
            console.log('fetchWeight_-->rr--> '+JSON.stringify(res));
        });
    }

    var numRegxForSpecial = (/^[0-9.]*$/);
    $scope.addWeightRecord = function() {
        if($scope.settingData.weightUnit=="Kg"){
            if($scope.user.kg=='' || $scope.user.kg==0) {
                customService.ToShowAlert('Please enter weight in kg.')
                return;
            }
            if($scope.user.kg=='.') {
                customService.ToShowAlert('Please enter valid weight in kg.')
                return;
            }
            if(($scope.user.kg!="" && !numRegxForSpecial.test($scope.user.kg)) || $scope.user.kg.split('.').length>2){
                customService.ToShowAlert('Please enter valid weight in kg.')
                return;
            }
            if($scope.user.recordate=='') {
                customService.ToShowAlert('Please select date for weight.')
                return;
            }
        }
        if($scope.settingData.weightUnit=="Lbs"){
            if($scope.user.weightLbs=='' || $scope.user.weightLbs==0) {
                customService.ToShowAlert('Please enter weight in lbs.')
                return;
            }
            if($scope.user.weightLbs==".") {
                customService.ToShowAlert('Please enter valid weight in lbs.')
                return;
            }
            if(($scope.user.weightLbs!="" && !numRegxForSpecial.test($scope.user.weightLbs)) || $scope.user.weightLbs.split('.').length>2){
                customService.ToShowAlert('Please enter valid weight in lbs.')
                return;
            }
            if($scope.user.recordate=='') {
                customService.ToShowAlert('Please select date for weight.')
                return;
            }
        }        
        if($scope.user.kg!='' && $scope.user.kg!=0){
            $scope.user.weightLbs = Math.floor($scope.user.kg*2.20462);
        }
        else if($scope.user.weightLbs!='' && $scope.user.weightLbs!=0){
            $scope.user.kg  = Math.floor($scope.user.weightLbs*0.453592);
        }
        $scope.user.recordate = $scope.user.recordate.getTime();
        user.insertWeight($scope.user).then(function(objS) {
            user.fetchLatestData("userWeight","recordDate").then(function(latSucc){
                //console.log("latSucc=>>  "+JSON.stringify(latSucc));
                user.updateData('users','server_userID',['weight','weighttype','weightDate', 'weightLbs', 'weightLbsType', 'updtDate', 'sync'],[latSucc.kgVal,'Kg',latSucc.recordDate,latSucc.lbsVal,'Lbs',new Date().getTime(),'0',$scope.user.userId]).then(function(succ) {
                    user.fetchUserInfo().then(function(userSucc){
                        customService.userDBInfo = userSucc;
                        //$state.reload();
                        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
                        $timeout(function(){
                            $ionicLoading.hide();
                            $state.reload();
                        },1000);
                    },function(userErr){
                        console.log(JSON.stringify(userErr));
                    });
                },function(err) {
                    console.log(JSON.stringify(err));
                });
            },function(latErr){
                console.log("latErr=>>  "+JSON.stringify(latErr));
            });            
        },function(objE){
        
        });
        $scope.lastNoteVal = ''
    }
    
    $scope.lastNoteVal = ''
    $scope.addNotes = function() {
        $scope.weightNote = $scope.lastNoteVal;
        var myPopup = $ionicPopup.show({
          template: '<div class="popup-content"><div class="textarea-block"><textarea placeholder="Notes" ng-model="weightNote"></textarea></div><div class="row btn-popup"><div class="col"><button class="button button-block btn-green" ng-click="saveNotes(weightNote)">Save</button></div><div class="col"><button class="button button-block btn-green" ng-click="closepop()"> Cancel </button></div></div></div>',
          scope: $scope,
          cssClass:'notes-popup'
        });
        $scope.closepop = function(){myPopup.close();}
        $scope.saveNotes = function(val){
            $scope.user.note = val;
            $scope.lastNoteVal = val;   
            $scope.notesLbl = false;
            $scope.editNotesLbl = true;
            myPopup.close();
        }
    
    }
    
    $scope.myGoBack = function() {
        //$ionicHistory.goBack();
        //console.log($rootScope.previousState);
        //$state.go($rootScope.previousState);
        if(customService.preStateForWHW=="menu"){
            $state.go('menu.home');
        }else if(customService.preStateForWHW=="personal"){
            $state.go('personal');
        }else{
            $state.go('personal');
        }
    }    
    
    $scope.weightNotes = function(data) {            
        $scope.savedNote = data;
        var myPopup = $ionicPopup.show({
          template: '<div class="popup-content"><div class="textarea-block"><textarea placeholder="Notes" ng-model="savedNote" readonly></textarea></div><div class="row btn-popup"><div class="col"><button class="button button-block btn-green" ng-click="closepop()">Close</button></div></div></div>',
          scope: $scope,
          cssClass:'notes-popup'
        });
        $scope.closepop = function(){myPopup.close();}
    }

    $scope.onHold = function(obj,ind){
        $scope.onHoldPressed = true;
        obj.selected = true;
        if(obj.selected == true)
            $scope.arrForShare.push(obj);
        //console.log("arrForShare->>  "+JSON.stringify($scope.arrForShare));
        if($scope.arrForShare.length>0){
            $scope.shareBtn=false;
            $scope.moreBtn=true;
        }else{
            $scope.shareBtn=true;
            $scope.moreBtn=false;
        }
    }

    $scope.onTap = function(obj,ind){
        //console.log("ontap called");
        if($scope.onHoldPressed == true)
            obj.selected==true?obj.selected=false:obj.selected=true;
        var isSelected = false;
        for(var i=0; i<$scope.weightData.length; i++){
            if($scope.weightData[i].selected==true){
                isSelected = true;
                break;
            }else{
                isSelected = false;
            }
        }
        isSelected == false?$scope.onHoldPressed = false:$scope.onHoldPressed = true;
        if(obj.selected == false){
            for(var i=0; i<$scope.arrForShare.length; i++){
                if($scope.arrForShare[i].recordDate==obj.recordDate){
                    $scope.arrForShare.splice($scope.arrForShare.indexOf($scope.arrForShare[i]), 1);
                }
            }
        }else{
            $scope.arrForShare.push(obj);
        }
        var anySelected = false;
        for(var i=0; i<$scope.weightData.length; i++){
            if($scope.weightData[i].selected==true){
                anySelected = true;
                break;
            }else{
                anySelected = false;
            }
        }
        if(anySelected==true){
            $scope.shareBtn=false;
            $scope.moreBtn=true;
        }else{
            $scope.shareBtn=true;
            $scope.moreBtn=false;
        }
        //console.log("arrForShare->>  "+JSON.stringify($scope.arrForShare));
    }

    /* Open popover on more option */
    $ionicPopover.fromTemplateUrl('popover.html', {
        scope: $scope
    }).then(function(popover) {
        $scope.popover = popover;
    });

    $scope.openShareOpt = function($event){
        $scope.popover.show($event);
    }

    $scope.actionPerform = function(act){
        console.log("action->>  "+act);
        if(act=="Delete"){
            //console.log("delSelectedArr->>  "+JSON.stringify($scope.weightData));  
            var statement = "";
            for(var i=0; i<$scope.weightData.length; i++){
                if($scope.weightData[i].selected==true)
                    statement = statement + $scope.weightData[i].uniId+",";
            }    
            statement = statement.substring(0,statement.length-1); 
            var query = "update userWeight set status='inactive', updtDate="+new Date().getTime()+", sync='0' where uniId in ("+statement+")";
            user.updateMulRowsAndColm(query).then(function(objS){
                $scope.onHoldPressed = false;
                $scope.shareBtn = true;
                $scope.moreBtn = false;
                $scope.getWeightData();
                $scope.popover.hide();
                user.getFirstRowFromTbl('userWeight').then(function(objS){
                    console.log("userWeight_firstRow->> "+JSON.stringify(objS));
                    if(objS.length>0){
                        user.updateData('users','server_userID',['weight','weighttype','weightLbs','weightLbsType','weightDate', 'updtDate', 'sync'],[objS[0].kgVal,'kg',objS[0].lbsVal,'Lbs',objS[0].recordDate,new Date().getTime(),"0",$scope.user.userId]).then(function(succ) {
                            console.log("updateData->> "+JSON.stringify(succ));
                            user.fetchUserInfo().then(function(userSucc){
                                customService.userDBInfo = userSucc;
                            },function(userErr){
                                console.log(JSON.stringify(userErr));
                            });
                        },function(err) {
                            console.log(JSON.stringify(err));
                        });
                    }else{
                        user.updateData('users','server_userID',['weight','weighttype','weightLbs','weightLbsType','weightDate', 'updtDate', 'sync'],['','','','','',new Date().getTime(),"0",$scope.user.userId]).then(function(succ) {
                            console.log("updateData->> "+JSON.stringify(succ));
                            user.fetchUserInfo().then(function(userSucc){
                                customService.userDBInfo = userSucc;
                            },function(userErr){
                                console.log(JSON.stringify(userErr));
                            });
                        },function(err) {
                            console.log(JSON.stringify(err));
                        });
                    }
                },function(objE){
                    console.log("userHeight_firstRow->> "+JSON.stringify(objE));
                });
            },function(objE){
            });            
        }
        if(act=="SelectAll"){
            for(var i=0; i<$scope.weightData.length; i++){
                $scope.weightData[i].selected = true;
            }
            $scope.popover.hide();
        }
        if(act=="DeselectAll"){
            for(var i=0; i<$scope.weightData.length; i++){
                $scope.weightData[i].selected = false;
            }
            $scope.shareBtn = true;
            $scope.moreBtn = false;
            $scope.onHoldPressed = false;
            $scope.popover.hide();
        }
        if(act=="Share"){
            console.log("shareAllArr->>  "+JSON.stringify($scope.arrForShare));
            $scope.popover.hide();
        }        
    }

    $scope.compareDate = function(){
        console.log("fromDate->> "+$scope.user.fromDate);
        console.log("toDate->> "+$scope.user.toDate);        
        if($scope.user.toDate!=0 &&($scope.user.fromDate>$scope.user.toDate)){
            customService.ToShowAlert('To date must be greater than or equal to from date.');
            //$scope.user.toDate = 0;
            $scope.user.toDate = undefined;
            $scope.user.fromDate = undefined;
            $state.reload();
            return;
        }
        if($scope.user.fromDate!=0 && $scope.user.toDate>=$scope.user.fromDate){
            console.log("done");
            user.selectWeightData('userWeight',$scope.user.fromDate.getTime(),$scope.user.toDate.getTime()).then(function(objS){
                console.log("selectWeightData_succ->>  "+JSON.stringify(objS));
                $scope.weightData = objS;
                $scope.graphRecord = [['Entry', 'Weight']];
                for(i=0;i<$scope.weightData.length;i++) {
                    var timeValue=new Date($scope.weightData[i].recordDate);
                    var timeFormat=timeValue.getDate()+'-'+(timeValue.getMonth()+1)+'-'+timeValue.getFullYear();
                    $scope.graphRecord.push([timeFormat,parseInt($scope.weightData[i].kg)]);
                }
                for(var i=0; i<$scope.weightData.length; i++){
                    $scope.weightData[i].selected = false;
                }
                //console.log('Weight Data Arr->> '+JSON.stringify($scope.weightData));
                if($scope.graphRecord.length>1) {
                    $timeout(function(){
                        google.charts.load('current', {'packages':['corechart']});
                        google.charts.setOnLoadCallback(drawChart);                    
                    },500);
                }
                function drawChart() {
                    var data = google.visualization.arrayToDataTable($scope.graphRecord);
                    var options = {
                        title: 'Weight Record',
                        hAxis: {title: 'Time',  
                            titleTextStyle: {color: '#333'},
                            direction: -1, 
                            slantedText: true, 
                            slantedTextAngle: 90
                        },
                        vAxis: {title: 'Kg',minValue: 0},
                        legend: {position: 'none'}
                    };
                    var chart = new google.visualization.AreaChart(document.getElementById('chart_div'));
                    chart.draw(data, options);
                }
            },function(objE){
                console.log("selectWeightData_err->>  "+JSON.stringify(objE));
            });
        }
    }

    /* Mobiscroll Timepicker functionality */
    $scope.settings_date = {
        theme: 'mobiscroll',
        //display: 'bottom',
        max: new Date(),
        dateFormat : "dd/mm/yy",
        timeFormat: 'hh:ii A',
        timeWheels: 'hhii A',
        animate: false,
        steps: { minute: 1, zeroBased: true },
        onSet:function(){
            var id=$('.date-time_bp-P').attr('id');
            $('#'+id)[0].value=$filter('dateWithTime')($scope.user.recordate.getTime());
        }
    };

    $scope.settings_from = {
        theme: 'mobiscroll',
        //display: 'bottom',
        max: new Date(),
        dateFormat : "dd/mm/yy",
        timeFormat: 'hh:ii A',
        timeWheels: 'hhii A',
        animate: false,
        steps: { minute: 1, zeroBased: true },
        onSet:function(){
            console.log("from->>  "+$scope.user.fromDate);
            var id=$('.date-time_picker_from').attr('id');
            $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
            $scope.compareDate();
        }
    };

    $scope.settings_to = {
        theme: 'mobiscroll',
        //display: 'bottom',
        max: new Date(),
        dateFormat : "dd/mm/yy",
        timeFormat: 'hh:ii A',
        timeWheels: 'hhii A',
        animate: false,
        steps: { minute: 1, zeroBased: true },
        onSet:function(){
            console.log("to->>  "+$scope.user.toDate);
            var id=$('.date-time_picker_to').attr('id');
            $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
            $scope.compareDate();
        }
    };

    $scope.quickFilter = function(type){
        console.log(type)
        if(type=="1wk"){
            $scope.oneWFilter = true;
            $scope.oneMFilter = false;
            $scope.threeMFilter = false;
            $scope.sixMFilter = false;
            $scope.oneYFilter = false;
            var oneWeekAgo = new Date();
            oneWeekAgo.setDate(oneWeekAgo.getDate() - 7);
            $scope.user.fromDate = oneWeekAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date-time_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date-time_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);       
        }
        if(type=="1mo"){
            $scope.oneWFilter = false;
            $scope.oneMFilter = true;
            $scope.threeMFilter = false;
            $scope.sixMFilter = false;
            $scope.oneYFilter = false;
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 1);
            $scope.user.fromDate = oneMonthAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date-time_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date-time_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);
        }
        if(type=="3mo"){
            $scope.oneWFilter = false;
            $scope.oneMFilter = false;
            $scope.threeMFilter = true;
            $scope.sixMFilter = false;
            $scope.oneYFilter = false;
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 3);
            $scope.user.fromDate = oneMonthAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date-time_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date-time_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);
        }
        if(type=="6mo"){
            $scope.oneWFilter = false;
            $scope.oneMFilter = false;
            $scope.threeMFilter = false;
            $scope.sixMFilter = true;
            $scope.oneYFilter = false;
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 6);
            $scope.user.fromDate = oneMonthAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date-time_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date-time_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);
        }
        if(type=="1yr"){
            $scope.oneWFilter = false;
            $scope.oneMFilter = false;
            $scope.threeMFilter = false;
            $scope.sixMFilter = false;
            $scope.oneYFilter = true;
            var oneMonthAgo = new Date();
            oneMonthAgo.setMonth(oneMonthAgo.getMonth() - 12);
            $scope.user.fromDate = oneMonthAgo;
            $scope.user.toDate = new Date();
            $timeout(function(){
                var id=$('.date-time_picker_to').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.toDate.getTime());
                var id=$('.date-time_picker_from').attr('id');
                $('#'+id)[0].value=$filter('dateWithTime')($scope.user.fromDate.getTime());
                $scope.compareDate();
            },10);
        }
    }

    $scope.clearAllFilters = function(){
        $scope.oneWFilter = false;
        $scope.oneMFilter = false;
        $scope.threeMFilter = false;
        $scope.sixMFilter = false;
        $scope.oneYFilter = false;
        $scope.user.toDate = undefined;
        $scope.user.fromDate = undefined;
        $scope.getWeightData();
    }

    $rootScope.hwBackBtnPressed = function(){
        console.log("backBtnPressed_called->>>  ");
        $(".mbsc-fr-btn1").click();
        $scope.myGoBack();
    }
});








