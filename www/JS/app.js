angular.module('PatientRecord', ['ionic','ngCordova',
     'ui.bootstrap.datetimepicker',
     'PatientRecord.loginCtrl',
     'PatientRecord.menuCtrl',
     'PatientRecord.homeCtrl',
     'PatientRecord.forgetPassCtrl',
     'PatientRecord.signupCtrl',
     'PatientRecord.OTPverifyCtrl',
     'PatientRecord.basicProfileCtrl',
     'PatientRecord.changePasswordCtrl',
     'PatientRecord.addAllergyCtrl',
     'PatientRecord.preExistConditionCtrl',
     'PatientRecord.relativeHealthConditionCtrl',
     'PatientRecord.termsConditionsCtrl',
     'PatientRecord.privacyPolicyCtrl',
     'PatientRecord.emergencyDetailCtrl',
     'PatientRecord.settingCtrl',
     'PatientRecord.resetPasswordCtrl',
     'PatientRecord.changePinCtrl',
     'PatientRecord.changeMobileCtrl',
     'PatientRecord.customService',
     'PatientRecord.calenderCtrl',
     'PatientRecord.timelineCtrl',
     'PatientRecord.shareCtrl',
     'PatientRecord.uploadCtrl',   
     'PatientRecord.localDB',
     'PatientRecord.diseases',
     'PatientRecord.personalCtrl',
     'PatientRecord.providerCtrl',
     'PatientRecord.healthConditionsCtrl',
     'PatientRecord.medicationCtrl',
     'PatientRecord.allergyPatientCtrl',
     'PatientRecord.vaccineDocumentCtrl',
     'PatientRecord.vaccinationDetailCtrl',
     'PatientRecord.vitalSymptomsCtrl',
     'PatientRecord.emergencyContactCtrl',
     'PatientRecord.familyFriendCtrl',
     'PatientRecord.reportPrescriptionCtrl',
     'PatientRecord.editPersonInfoCtrl',
     'PatientRecord.weightCtrl',
     'PatientRecord.heightCtrl',
     'PatientRecord.waistCtrl',
     'PatientRecord.additionInformationCtrl',
     'PatientRecord.additionalEditInformationCtrl',
     'PatientRecord.addProviderCtrl',
     'PatientRecord.doctorProfileCtrl',
     'PatientRecord.myQRCodeCtrl',
     'PatientRecord.addMedicationCtrl',
     'PatientRecord.shareToCtrl',
     'PatientRecord.medicationDetailCtrl',
     'mobiscroll-datetime', 
     'angular-carousel',
     'ngFileUpload',
     'PatientRecord.titleCtrl',
     'PatientRecord.addDocumentionCtrl',
     'PatientRecord.addVaccinationCtrl',
     'naif.base64',
     'mwl.calendar',
     'ui.bootstrap',
     'colorpicker.module',
     'PatientRecord.VS_bloodPressureCtrl',
     'PatientRecord.VS_bloodSugarCtrl',
     'PatientRecord.VS_eyePowerCtrl',
     'PatientRecord.VS_temperatureCtrl',
     'PatientRecord.docs',
     'PatientRecord.addSubProfileCtrl',
     'PatientRecord.editSubProfileCtrl',
     'PatientRecord.sharingSer',
     'PatientRecord.sync',
     'PatientRecord.doctor',
     'PatientRecord.emergencyContactViewCtrl',
     /* Doctor Section */
     'PatientRecord.dr_editProfileCtrl',
     'PatientRecord.dr_doctorProfileCtrl',
     'PatientRecord.dr_patientListCtrl',
     'PatientRecord.dr_menuCtrl',
     'PatientRecord.dr_addProviderCtrl',
     'PatientRecord.dr_myProviderCtrl',
     'PatientRecord.dr_uploadCtrl',
     'PatientRecord.dr_settingCtrl',
     'PatientRecord.dr_patientDetailsCtrl',
     'PatientRecord.dr_documentListCtrl',
     'PatientRecord.dr_addDocumentCtrl',
     'PatientRecord.dr_docDetailCtrl',
     'PatientRecord.dr_ePrescriptionCtrl',
     'PatientRecord.dr_addVitalsCtrl',
     'PatientRecord.dr_viewEPrescriptionCtrl',
     'PatientRecord.dr_addPatientCtrl',
     'PatientRecord.dr_addMedicineCtrl',
     'PatientRecord.dr_PatientPersonalCtrl',
     'PatientRecord.dr_PatientHelCondCtrl',
     'PatientRecord.dr_PatientMedicationCtrl',
     'PatientRecord.dr_PatientAllergyCtrl',
     'PatientRecord.dr_PatientDocVaccCtrl',
     'PatientRecord.dr_PatientVSCtrl',
     'PatientRecord.dr_PatientRSPCtrl',
     'PatientRecord.dr_PatientEmerContactCtrl',
     'PatientRecord.dr_ProviderDetailsCtrl',
     'PatientRecord.dr_editProfileAfterSignupCtrl',
     'PatientRecord.dr_viewProfileAfterSignupCtrl',
     'PatientRecord.dr_shareCtrl',
     'PatientRecord.dr_shareToCtrl',
     'PatientRecord.dr_timelineCtrl',
     'PatientRecord.dr_VS_bloodPressureCtrl',
     'PatientRecord.dr_VS_bloodSugarCtrl',
     'PatientRecord.dr_VS_eyePowerCtrl',
     'PatientRecord.dr_VS_temperatureCtrl',
     'PatientRecord.dr_heightCtrl',
     'PatientRecord.dr_waistCtrl',
     'PatientRecord.dr_weightCtrl',
     'PatientRecord.dr_PatientRSPDetCtrl',
     'PatientRecord.dr_uploadViewCtrl',
     'PatientRecord.dr_patientAdditionalInfoCtrl'
])

.run(function($ionicPlatform,$state,localDB, $cordovaNetwork, user, disease, customService, $cordovaDevice, $rootScope,DBInstance, $ionicHistory, sharingSer, $ionicLoading, sync, doctor) {
    
    $ionicPlatform.ready(function() {
        // Check for network connection on App start
        if($cordovaNetwork.isOnline() == true){
          customService.connection=true;
        }else{
          customService.connection=false;
        }

        localDB.createDB().then(function(DBSucc){
            console.log('DB SUCCESS');
            if(customService.connection == true)
                sync.syncIndeData();
        },function(DBErr){
            console.log('DB ERROR');
        })

        var platform = $cordovaDevice.getPlatform().toLowerCase();
        localStorage.setItem('device_type',platform);
        console.log('db directory -> '+cordova.file.dataDirectory);

        //initApp();
        $ionicLoading.show({template: '<ion-spinner class="spinner-energized" icon="bubbles"></ion-spinner><p>Please wait....</p>'});
        window.requestFileSystem(LocalFileSystem.PERSISTENT, 0, function (fileSystem) {
            customService.fs = fileSystem;
            initApp();
            console.log("filesystem: "+JSON.stringify(customService.fs));
        }, function(err){
            $ionicLoading.hide();
            console.log("errorwhile opening file system: ");
        });
        
        function initApp(){
            if(localStorage.getItem('loginStatus')=='Yes') {
                customService.carouselUserId=parseInt(localStorage.getItem('loginUserId'));
                console.log('carouselUserId app js => '+customService.carouselUserId);
                user.fetchUserInfo().then(function(userDataSucc){
                    customService.userDBInfo = userDataSucc;
                    if(customService.userDBInfo.role == "2"){
                        if(customService.connection == true){
                            doctor.checkDrApprovedByAdmin(customService.userDBInfo.userId).then(function(drSucc){
                                //console.log('drSucc=>> '+JSON.stringify(drSucc));
                                if(drSucc.data.responseCode==200){
                                    var obj={
                                        id:customService.userDBInfo.userId,
                                        varified_time:drSucc.data.responseData.varified_time
                                    }
                                    if(drSucc.data.responseData.profile_verified==1 || drSucc.data.responseData.profile_verified=="1")
                                        obj.verify_status="APPROVED";
                                    else
                                        obj.verify_status="PENDING";
                                    doctor.updateDoctorDet(obj).then(function(objSucc){
                                        console.log('update success appjs');
                                        doctor.getDoctorDet(customService.userDBInfo.userId).then(function(docDetSucc){
                                            $ionicLoading.hide();
                                            if(docDetSucc == '')
                                                $state.go('dr_viewProfileAfterSignup');
                                            else{
                                                if(docDetSucc.verify_status == "PENDING")
                                                    $state.go('dr_viewProfileAfterSignup');
                                                else{
                                                    if(customService.connection == true){
                                                        sync.syncDepData();
                                                        //sync.synOnlyDoc();
                                                        //$state.go('dr_menu.dr_patientList');
                                                        doctor.insertPatientList(customService.userDBInfo.userId).then(function(insSucc){
                                                            $state.go('dr_menu.dr_patientList');
                                                        },function(insErr){});
                                                    }else
                                                        $state.go('dr_menu.dr_patientList');
                                                }                                    
                                            }                                          
                                        },function(docDetErr){
                                            $ionicLoading.hide();
                                            console.log("getDoctorDet_err");
                                        });
                                    },function(objErr){
                                        $ionicLoading.hide();
                                        console.log('update error');
                                    });                                    
                                }else{
                                    console.log("status_err");
                                    $ionicLoading.hide();
                                }                                            
                            },function(drErr){
                                $ionicLoading.hide();
                                console.log('checkDrApprovedByAdmin error');
                            });
                        }else{
                            doctor.getDoctorDet(customService.userDBInfo.userId).then(function(docDetSucc){
                                $ionicLoading.hide();
                                if(docDetSucc == '')
                                    $state.go('dr_viewProfileAfterSignup');
                                else{
                                    if(docDetSucc.verify_status == "PENDING")
                                        $state.go('dr_viewProfileAfterSignup');
                                    else{
                                        if(customService.connection == true){
                                            sync.syncDepData();
                                            sync.synOnlyDoc();
                                        }
                                        $state.go('dr_menu.dr_patientList');
                                    }                                    
                                }
                            },function(docDetErr){
                                $ionicLoading.hide();
                            });
                        }                        
                    }else if(customService.userDBInfo.role == "3"){
                        if(customService.connection == true){
                            sync.syncDepData();
                            sync.synOnlyPat();
                        }
                        disease.getPatientSettingData(localStorage.getItem('loginUserId')).then(function(objS){ 
                            $ionicLoading.hide();
                            if(objS.statusforEmerDet=="1"){
                                $state.go('emergencyDetail');
                            }else{
                                $state.go('menu.home');
                            }
                        },function(objE){
                            $ionicLoading.hide();
                            console.log("getPatientSettingData_err->>  "+JSON.stringify(objE));
                        });
                    }
                },function(userDataErr){
                    $ionicLoading.hide();
                    console.log('app_fetchUserInfo_err:-- '+JSON.stringify(objE));
                })
            } else {
                $ionicLoading.hide();
                $state.go('login');
            }
        }

        // listen for Online event 
        $rootScope.$on('$cordovaNetwork:online', function(event, networkState){
            var onlineState = networkState;
            if(onlineState == 'none'){
                customService.connection=false;
            }else{
                customService.connection=true;
                sync.syncIndeData();
                if(localStorage.getItem('loginStatus')=='Yes') {
                    sync.syncDepData();
                    if(customService.userDBInfo.role == "2")
                        sync.synOnlyDoc();
                    else if(customService.userDBInfo.role == "3")
                        sync.synOnlyPat();
                }
            }
        })

        // listen for Offline event
        $rootScope.$on('$cordovaNetwork:offline', function(event, networkState){
            var offlineState = networkState;
            if(offlineState == 'none'){
                customService.connection=false;
            }else{
                customService.connection=true;
            }
        })

        if (window.cordova && window.cordova.plugins.Keyboard) {
            cordova.plugins.Keyboard.hideKeyboardAccessoryBar(false);
            cordova.plugins.Keyboard.disableScroll(true);
        }
        if (window.StatusBar) {
            StatusBar.styleDefault();
        }
    });
    $rootScope.previousState;
    $rootScope.currentState;
    $rootScope.$on('$stateChangeSuccess', function(ev, to, toParams, from, fromParams) {
        console.log('changed state');
        $rootScope.previousState = from.name; $rootScope.currentState = to.name;
    });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {
    $ionicConfigProvider.views.swipeBackEnabled(false);
    $stateProvider.state('menu', {
        url: '/menu',
        abstract: true,
        templateUrl: 'PatientRecord/menu/menu.html',
        controller: 'menuCtrl'
    })

    .state('login', {
        url: '/login',
        templateUrl: 'PatientRecord/login/login.html',
        controller: 'loginCtrl'
    })
    
    .state('forget', {
        url: '/forget',
        templateUrl: 'PatientRecord/forgetPass/forgetPass.html',
        controller: 'forgetPassCtrl'
    })
        
    .state('signup', {
        url: '/signup',
        templateUrl: 'PatientRecord/signup/signup.html',
        controller: 'signupCtrl'
    })
        
    .state('basicProfile', {
        url: '/basicProfile',
        templateUrl: 'PatientRecord/basicProfile/basicProfile.html',
        controller: 'basicProfileCtrl'
    })
        
    .state('OTPverify', {
        url: '/OTPverify',
        templateUrl: 'PatientRecord/OTPverify/OTPverify.html',
        controller: 'OTPverifyCtrl'
    })
        
    .state('resetPassword', {
        url: '/resetPassword',
        templateUrl: 'PatientRecord/resetPassword/resetPassword.html',
        controller: 'resetPasswordCtrl'
    })
    
    .state('addAllergy', {
        url: '/addAllergy',
        templateUrl: 'PatientRecord/addAllergy/addAllergy.html',
        controller: 'addAllergyCtrl'
    })
        
    .state('preExistCondition', {
        url: '/preExistCondition',
        templateUrl: 'PatientRecord/preExistCondition/preExistCondition.html',
        controller: 'preExistConditionCtrl'
    })
        
    .state('relativeHealthCondition', {
        url: '/relativeHealthCondition',
        templateUrl: 'PatientRecord/relativeHealthCondition/relativeHealthCondition.html',
        controller: 'relativeHealthConditionCtrl'
    })
        
    .state('termsConditions', {
        url: '/termsConditions',
        templateUrl: 'PatientRecord/termsConditions/termsConditions.html',
        controller: 'termsConditionsCtrl'
    })
        
    .state('privacyPolicy', {
        url: '/privacyPolicy',
        templateUrl: 'PatientRecord/privacyPolicy/privacyPolicy.html',
        controller: 'privacyPolicyCtrl'
    })
        
    .state('emergencyDetail', {
        url: '/emergencyDetail',
        templateUrl: 'PatientRecord/emergencyDetail/emergencyDetail.html',
        controller: 'emergencyDetailCtrl'
    })
        
    .state('setting', {
        url: '/setting',
        templateUrl: 'PatientRecord/setting/setting.html',
        controller: 'settingCtrl'
    })
        
    .state('changePassword', {
        url: '/changePassword',
        templateUrl: 'PatientRecord/changePassword/changePassword.html',
        controller: 'changePasswordCtrl'
    })
        
    .state('changePin', {
        url: '/changePin',
        templateUrl: 'PatientRecord/changePin/changePin.html',
        controller: 'changePinCtrl'
    })
        
    .state('changeMobile', {
        url: '/changeMobile',
        templateUrl: 'PatientRecord/changeMobile/changeMobile.html',
        controller: 'changeMobileCtrl'
    })
        
    .state('calender', {
        url: '/calender',
        templateUrl: 'PatientRecord/calender/calender.html',
        controller: 'calenderCtrl'
    })
        
    .state('timeline', {
        url: '/timeline',
        templateUrl: 'PatientRecord/timeline/timeline.html',
        controller: 'timelineCtrl'
    })
        
    .state('share', {
        url: '/share',
        templateUrl: 'PatientRecord/share/share.html',
        controller: 'shareCtrl'
    })
        
    .state('upload', {
        url: '/upload',
        templateUrl: 'PatientRecord/upload/upload.html',
        controller: 'uploadCtrl'
    })
      
    .state('personal', {
        url: '/personal',
        templateUrl: 'PatientRecord/personal/personal.html',
        controller: 'personalCtrl'
    })
    
    .state('provider', {
        url: '/provider',
        templateUrl: 'PatientRecord/provider/provider.html',
        controller: 'providerCtrl'
    })
        
    .state('healthConditions', {
        url: '/healthConditions',
        templateUrl: 'PatientRecord/healthConditions/healthConditions.html',
        controller: 'healthConditionsCtrl'
    })
        
    .state('medication', {
        url: '/medication',
        templateUrl: 'PatientRecord/medication/medication.html',
        controller: 'medicationCtrl'
    })
        
    .state('allergyPatient', { 
        url: '/allergyPatient',
        templateUrl: 'PatientRecord/allergyPatient/allergyPatient.html',
        controller: 'allergyPatientCtrl'
    })
        
    .state('vaccineDocument', {
        url: '/vaccineDocument',
        templateUrl: 'PatientRecord/vaccineDocument/vaccineDocument.html',
        controller: 'vaccineDocumentCtrl'
    })
    
    .state('vaccinationDetail', {
        url: '/vaccinationDetail',
        templateUrl: 'PatientRecord/vaccinationDetail/vaccinationDetail.html',
        controller: 'vaccinationDetailCtrl'
    })
        
    .state('vitalSymptoms', {
        url: '/vitalSymptoms',
        templateUrl: 'PatientRecord/vitalSymptoms/vitalSymptoms.html',
        controller: 'vitalSymptomsCtrl'
    })
      
    .state('emergencyContact', {
        url: '/emergencyContact',
        templateUrl: 'PatientRecord/emergencyContact/emergencyContact.html',
        controller: 'emergencyContactCtrl'
    })
        
    .state('familyFriend', {
        url: '/familyFriend',
        templateUrl: 'PatientRecord/familyFriend/familyFriend.html',
        controller: 'familyFriendCtrl'
    })
        
    .state('reportPrescription', {
        url: '/reportPrescription',
        templateUrl: 'PatientRecord/reportPrescription/reportPrescription.html',
        controller: 'reportPrescriptionCtrl'
    })
        
    .state('editPersonInfo', {
        url: '/editPersonInfo',
        templateUrl: 'PatientRecord/editPersonInfo/editPersonInfo.html',
        controller: 'editPersonInfoCtrl'
    })
        
    .state('height', {
        url: '/height',
        templateUrl: 'PatientRecord/height/height.html',
        controller: 'heightCtrl'
    })
        
    .state('waist', {
        url: '/waist',
        templateUrl: 'PatientRecord/waist/waist.html',
        controller: 'waistCtrl'
    })
        
    .state('weight', {
        url: '/weight',
        templateUrl: 'PatientRecord/weight/weight.html',
        controller: 'weightCtrl'
    })
        
    .state('additionInformation', {
        url: '/additionInformation',
        templateUrl: 'PatientRecord/additionInformation/additionInformation.html',
        controller: 'additionInformationCtrl'
    })
        
    .state('additionalEditInformation', {
        url: '/additionalEditInformation',
        templateUrl: 'PatientRecord/additionalEditInformation/additionalEditInformation.html',
        controller: 'additionalEditInformationCtrl'
    })
        
    .state('addProvider', {
        url: '/addProvider',
        templateUrl: 'PatientRecord/addProvider/addProvider.html',
        controller: 'addProviderCtrl'
    })
        
    .state('doctorProfile', {
        url: '/doctorProfile',
        templateUrl: 'PatientRecord/doctorProfile/doctorProfile.html',
        controller: 'doctorProfileCtrl'
    })
        
    .state('myQRCode', {
        url: '/myQRCode',
        templateUrl: 'PatientRecord/myQRCode/myQRCode.html',
        controller: 'myQRCodeCtrl'
    })
        
    .state('addMedication', {
        url: '/addMedication',
        templateUrl: 'PatientRecord/addMedication/addMedication.html',
        controller: 'addMedicationCtrl'
    })
        
    .state('shareTo', {
        url: '/shareTo',
        templateUrl: 'PatientRecord/shareTo/shareTo.html',
        controller: 'shareToCtrl'
    })
        
    .state('medicationDetail', {
        url: '/medicationDetail',
        templateUrl: 'PatientRecord/medicationDetail/medicationDetail.html',
        controller: 'medicationDetailCtrl'
    })
        
    .state('menu.home', {
        url: '/home',
        views: {
            'menuContent': {
                templateUrl: 'PatientRecord/home/home.html',
                controller: 'homeCtrl'
            }
        }
    })
    
    .state('title', {
        url: '/title',
        templateUrl: 'PatientRecord/title/title.html',
        controller: 'titleCtrl'
    })
    .state('addDocumention', {
        url: '/title',
        templateUrl: 'PatientRecord/addDocumention/addDocumention.html',
        controller: 'addDocumentionCtrl'
    })
    .state('addVaccination', {
        url: '/addVaccination',
        templateUrl: 'PatientRecord/addVaccination/addVaccination.html',
        controller: 'addVaccinationCtrl'
    })
    
    .state('VS_bloodPressure', {
        url: '/VS_bloodPressure',
        templateUrl: 'PatientRecord/VS_bloodPressure/VS_bloodPressure.html',
        controller: 'VS_bloodPressureCtrl'
    })
    .state('VS_bloodSugar', {
        url: '/VS_bloodSugar',
        templateUrl: 'PatientRecord/VS_bloodSugar/VS_bloodSugar.html',
        controller: 'VS_bloodSugarCtrl'
    })
    .state('VS_eyePower', {
        url: '/VS_eyePower',
        templateUrl: 'PatientRecord/VS_eyePower/VS_eyePower.html',
        controller: 'VS_eyePowerCtrl'
    })
    .state('VS_temperature', {
        url: '/VS_temperature',
        templateUrl: 'PatientRecord/VS_temperature/VS_temperature.html',
        controller: 'VS_temperatureCtrl'
    })
    .state('addSubProfile', {
        url: '/addSubProfile',
        templateUrl: 'PatientRecord/addSubProfile/addSubProfile.html',
        controller: 'addSubProfileCtrl'
    })
    .state('editSubProfile', {
        url: '/editSubProfile',
        templateUrl: 'PatientRecord/editSubProfile/editSubProfile.html',
        controller: 'editSubProfileCtrl'
    })
    .state('emergencyContactView', {
        url: '/emergencyContactView',
        templateUrl: 'PatientRecord/emergencyContactView/emergencyContactView.html',
        controller: 'emergencyContactViewCtrl'
    })

    /* Doctor Section */
    .state('dr_menu', {
        url: '/dr_menu',
        abstract: true,
        templateUrl: 'DoctorRecord/dr_menu/dr_menu.html',
        controller: 'dr_menuCtrl'
    })
    .state('dr_editProfile', {
        url: '/dr_editProfile',
        templateUrl: 'DoctorRecord/dr_editProfile/dr_editProfile.html',
        controller: 'dr_editProfileCtrl'
    })
    .state('dr_doctorProfile', {
        url: '/dr_doctorProfile',
        templateUrl: 'DoctorRecord/dr_doctorProfile/dr_doctorProfile.html',
        controller: 'dr_doctorProfileCtrl'
    })
    .state('dr_menu.dr_patientList', {
        url: '/dr_patientList',
        views: {
            'drMenuContent': {
                templateUrl: 'DoctorRecord/dr_patientList/dr_patientList.html',
                controller: 'dr_patientListCtrl'
            }
        }
    })
    .state('dr_addProvider', {
        url: '/dr_addProvider',
        templateUrl: 'DoctorRecord/dr_addProvider/dr_addProvider.html',
        controller: 'dr_addProviderCtrl'
    })
    .state('dr_myProvider', {
        url: '/dr_myProvider',
        templateUrl: 'DoctorRecord/dr_myProvider/dr_myProvider.html',
        controller: 'dr_myProviderCtrl'
    })
    .state('dr_upload', {
        url: '/dr_upload',
        templateUrl: 'DoctorRecord/dr_upload/dr_upload.html',
        controller: 'dr_uploadCtrl'
    })
    .state('dr_setting', {
        url: '/dr_setting',
        templateUrl: 'DoctorRecord/dr_setting/dr_setting.html',
        controller: 'dr_settingCtrl'
    })
    .state('dr_patientDetails', {
        url: '/dr_patientDetails',
        templateUrl: 'DoctorRecord/dr_patientDetails/dr_patientDetails.html',
        controller: 'dr_patientDetailsCtrl'
    })
    .state('dr_documentList', {
        url: '/dr_documentList',
        templateUrl: 'DoctorRecord/dr_documentList/dr_documentList.html',
        controller: 'dr_documentListCtrl'
    })
    .state('dr_addDocument', {
        url: '/dr_addDocument',
        templateUrl: 'DoctorRecord/dr_addDocument/dr_addDocument.html',
        controller: 'dr_addDocumentCtrl'
    })
    .state('dr_docDetail', {
        url: '/dr_docDetail',
        templateUrl: 'DoctorRecord/dr_docDetail/dr_docDetail.html',
        controller: 'dr_docDetailCtrl'
    })
    .state('dr_ePrescription', {
        url: '/dr_ePrescription',
        templateUrl: 'DoctorRecord/dr_ePrescription/dr_ePrescription.html',
        controller: 'dr_ePrescriptionCtrl'
    })
    .state('dr_addVitals', {
        url: '/dr_addVitals',
        templateUrl: 'DoctorRecord/dr_addVitals/dr_addVitals.html',
        controller: 'dr_addVitalsCtrl'
    })
    .state('dr_viewEPrescription', {
        url: '/dr_viewEPrescription',
        templateUrl: 'DoctorRecord/dr_viewEPrescription/dr_viewEPrescription.html',
        controller: 'dr_viewEPrescriptionCtrl'
    })
    .state('dr_addPatient', {
        url: '/dr_addPatient',
        templateUrl: 'DoctorRecord/dr_addPatient/dr_addPatient.html',
        controller: 'dr_addPatientCtrl'
    })
    .state('dr_addMedicine', {
        url: '/dr_addMedicine',
        templateUrl: 'DoctorRecord/dr_addMedicine/dr_addMedicine.html',
        controller: 'dr_addMedicineCtrl'
    })
    .state('dr_PatientPersonal', {
        url: '/dr_PatientPersonal',
        templateUrl: 'DoctorRecord/dr_PatientPersonal/dr_PatientPersonal.html',
        controller: 'dr_PatientPersonalCtrl'
    })
    .state('dr_PatientHelCond', {
        url: '/dr_PatientHelCond',
        templateUrl: 'DoctorRecord/dr_PatientHelCond/dr_PatientHelCond.html',
        controller: 'dr_PatientHelCondCtrl'
    })
    .state('dr_PatientMedication', {
        url: '/dr_PatientMedication',
        templateUrl: 'DoctorRecord/dr_PatientMedication/dr_PatientMedication.html',
        controller: 'dr_PatientMedicationCtrl'
    })
    .state('dr_PatientAllergy', {
        url: '/dr_PatientAllergy',
        templateUrl: 'DoctorRecord/dr_PatientAllergy/dr_PatientAllergy.html',
        controller: 'dr_PatientAllergyCtrl'
    })
    .state('dr_PatientDocVacc', {
        url: '/dr_PatientDocVacc',
        templateUrl: 'DoctorRecord/dr_PatientDocVacc/dr_PatientDocVacc.html',
        controller: 'dr_PatientDocVaccCtrl'
    })
    .state('dr_PatientVS', {
        url: '/dr_PatientVS',
        templateUrl: 'DoctorRecord/dr_PatientVS/dr_PatientVS.html',
        controller: 'dr_PatientVSCtrl'
    })
    .state('dr_PatientRSP', {
        url: '/dr_PatientRSP',
        templateUrl: 'DoctorRecord/dr_PatientRSP/dr_PatientRSP.html',
        controller: 'dr_PatientRSPCtrl'
    })
    .state('dr_PatientEmerContact', {
        url: '/dr_PatientEmerContact',
        templateUrl: 'DoctorRecord/dr_PatientEmerContact/dr_PatientEmerContact.html',
        controller: 'dr_PatientEmerContactCtrl'
    })
    .state('dr_ProviderDetails', {
        url: '/dr_ProviderDetails',
        templateUrl: 'DoctorRecord/dr_ProviderDetails/dr_ProviderDetails.html',
        controller: 'dr_ProviderDetailsCtrl'
    })
    .state('dr_editProfileAfterSignup', {
        url: '/dr_editProfileAfterSignup',
        templateUrl: 'DoctorRecord/dr_editProfileAfterSignup/dr_editProfileAfterSignup.html',
        controller: 'dr_editProfileAfterSignupCtrl'
    })
    .state('dr_viewProfileAfterSignup', {
        url: '/dr_viewProfileAfterSignup',
        templateUrl: 'DoctorRecord/dr_viewProfileAfterSignup/dr_viewProfileAfterSignup.html',
        controller: 'dr_viewProfileAfterSignupCtrl'
    })
    .state('dr_share', {
        url: '/dr_share',
        templateUrl: 'DoctorRecord/dr_share/dr_share.html',
        controller: 'dr_shareCtrl'
    })
    .state('dr_shareTo', {
        url: '/dr_shareTo',
        templateUrl: 'DoctorRecord/dr_shareTo/dr_shareTo.html',
        controller: 'dr_shareToCtrl'
    })
    .state('dr_timeline', {
        url: '/dr_timeline',
        templateUrl: 'DoctorRecord/dr_timeline/dr_timeline.html',
        controller: 'dr_timelineCtrl'
    })
    .state('dr_VS_bloodPressure', {
        url: '/dr_VS_bloodPressure',
        templateUrl: 'DoctorRecord/dr_VS_bloodPressure/dr_VS_bloodPressure.html',
        controller: 'dr_VS_bloodPressureCtrl'
    })
    .state('dr_VS_bloodSugar', {
        url: '/dr_VS_bloodSugar',
        templateUrl: 'DoctorRecord/dr_VS_bloodSugar/dr_VS_bloodSugar.html',
        controller: 'dr_VS_bloodSugarCtrl'
    })
    .state('dr_VS_eyePower', {
        url: '/dr_VS_eyePower',
        templateUrl: 'DoctorRecord/dr_VS_eyePower/dr_VS_eyePower.html',
        controller: 'dr_VS_eyePowerCtrl'
    })
    .state('dr_VS_temperature', {
        url: '/dr_VS_temperature',
        templateUrl: 'DoctorRecord/dr_VS_temperature/dr_VS_temperature.html',
        controller: 'dr_VS_temperatureCtrl'
    })
    .state('dr_height', {
        url: '/dr_height',
        templateUrl: 'DoctorRecord/dr_height/dr_height.html',
        controller: 'dr_heightCtrl'
    })
    .state('dr_waist', {
        url: '/dr_waist',
        templateUrl: 'DoctorRecord/dr_waist/dr_waist.html',
        controller: 'dr_waistCtrl'
    })
    .state('dr_weight', {
        url: '/dr_weight',
        templateUrl: 'DoctorRecord/dr_weight/dr_weight.html',
        controller: 'dr_weightCtrl'
    })
    .state('dr_PatientRSPDet', {
        url: '/dr_PatientRSPDet',
        templateUrl: 'DoctorRecord/dr_PatientRSPDet/dr_PatientRSPDet.html',
        controller: 'dr_PatientRSPDetCtrl'
    })
    .state('dr_uploadView', {
        url: '/dr_uploadView',
        templateUrl: 'DoctorRecord/dr_uploadView/dr_uploadView.html',
        controller: 'dr_uploadViewCtrl'
    })
    .state('dr_patientAdditionalInfo', {
        url: '/dr_patientAdditionalInfo',
        templateUrl: 'DoctorRecord/dr_patientAdditionalInfo/dr_patientAdditionalInfo.html',
        controller: 'dr_patientAdditionalInfoCtrl'
    })

    // if none of the above states are matched, use this as the fallback
    //$urlRouterProvider.otherwise('/login');
})




